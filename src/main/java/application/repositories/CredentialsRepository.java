package application.repositories;

import application.orders.Credentials;
import application.orders.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface CredentialsRepository extends JpaRepository<Credentials, UUID> {

}
