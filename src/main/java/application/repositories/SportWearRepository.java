package application.repositories;

import application.orders.Order;
import application.orders.ShoppingCart;
import application.wears.SportWear;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface SportWearRepository extends JpaRepository<SportWear, UUID> {

}
