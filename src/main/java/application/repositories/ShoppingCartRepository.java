package application.repositories;

import application.orders.Order;
import application.orders.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, String> {

}
