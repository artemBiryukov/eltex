package application;

import application.orders.Orders;
import application.orders.serialize.ManageOrderJson;

import java.io.FileNotFoundException;

public class ExceptionHandler {
    public static String deleteAndHandle(Orders orders, String command,String id,ManageOrderJson moj){
        //ManageOrderJson moj = new ManageOrderJson(orders);
        if(!orders.getOrders().contains(orders.findId(id))){
            return "1";
        }else if(!command.equals("delById")){
            return "3";
        }else{
            moj.deleteById(id);
        }
        return "0";
    }

}
