package application.orders;

import application.wears.SportWear;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.UUID;
@Entity
@Table(name="cart")
public class ShoppingCart<T extends SportWear> implements Serializable {
    @Id
    private String id;
    @OneToOne
    @JoinColumn(name="cred_id")
    private Credentials cred;
    @Transient
    private List<T> bucket = new ArrayList<>();
    @Transient
    private TreeSet<UUID> idOfOrder = new TreeSet<>();

    public ShoppingCart(){
        this.id = UUID.randomUUID().toString();
    }

    public Credentials getCred() {
        return cred;
    }

    public void setCred(Credentials cred) {
        this.cred = cred;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void add(T obj) {
        bucket.add(obj);
        idOfOrder.add(obj.id);
    }

    public void delete(T obj) {
        bucket.remove(obj);
        idOfOrder.remove(obj.id);
    }

    public void showAll() {
        for (T obj : bucket) {
            System.out.println(obj);
        }
    }

    public T findItem(UUID id) {
        for (T item : bucket) {
            if (item.id.equals(id)) {
                return item;
            }
        }
        return null;
    }
}
