package application.orders.serialize;

import application.orders.Order;

import java.util.LinkedList;
import java.util.UUID;

public interface IOrder {
    Order readById(String id);
    String saveById(String id);
    LinkedList<Order> readAll();
    String saveAll();

}
