package application.orders.serialize;

import application.orders.Order;
import application.orders.Orders;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.util.LinkedList;
import java.util.Optional;
import java.util.UUID;

public class ManageOrderFile extends AManageOrder {
    private static final String PATH_TO_TXT = "./src/main/java/application/orders/serialize/TXT.txt";
    private static File toTXT = new File(PATH_TO_TXT);
    @Autowired
    public Orders orders;
    public ManageOrderFile(Orders orders) {
        super(orders);
    }

    @Override
    public Order readById(String id) {
        LinkedList<Order> serOrders = null;
        Order order = null;
        try {
            FileInputStream fis = new FileInputStream(toTXT);
            ObjectInputStream ois = new ObjectInputStream(fis);
            serOrders = (LinkedList<Order>) ois.readObject();
            Optional<Order> orderOpt = serOrders.stream().filter(i -> ((Order) i).getId().toString().equals(id)).findFirst();
            if (orderOpt.isPresent()) {
                order = orderOpt.get();
            }
            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return order;
    }

    @Override
    public String saveById(String id) {
        LinkedList<Order> orders = new LinkedList<>();
        String s="";
        try {
            FileOutputStream fos = new FileOutputStream(toTXT);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            Optional<Order> orderOpt = this.orders.getOrders().stream().filter(i -> ((Order) i).getId().toString().equals(id)).findFirst();
            if (orderOpt.isPresent()) {
                Order order = (Order)orderOpt.get();
                s = order.getId().toString();
                orders.add(order);
                oos.writeObject(orders);
            }
            oos.close();
            fos.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    @Override
    public LinkedList<Order> readAll() {
        LinkedList<Order> serOrders = null;
        try {
            FileInputStream fis = new FileInputStream(toTXT);
            ObjectInputStream ois = new ObjectInputStream(fis);
            serOrders = (LinkedList<Order>) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return serOrders;
    }

    @Override
    public String saveAll() {
        String s="";
        try {
            FileOutputStream fos = new FileOutputStream(toTXT);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(orders.getOrders());
            s = oos.toString();
            oos.close();
            fos.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return s;
    }
}
