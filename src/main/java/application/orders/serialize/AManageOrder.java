package application.orders.serialize;

import application.orders.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public abstract class AManageOrder implements IOrder {
    protected Orders orders;
    @Autowired
    public AManageOrder(Orders orders){
        this.orders = orders;
    }
}
