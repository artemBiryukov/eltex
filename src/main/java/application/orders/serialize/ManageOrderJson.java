package application.orders.serialize;

import application.orders.Order;
import application.orders.Orders;
import application.wears.Cap;
import application.wears.SportWear;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;
@Component
public class ManageOrderJson extends AManageOrder {
    private static Logger logger = LoggerFactory.getLogger(ManageOrderJson.class);
    private static GsonBuilder gb = new GsonBuilder();
    private static Gson gson = gb.create();
    private static final String PATH_TO_JSON = "./src/main/java/application/orders/serialize/JSON.json";
    private static final File toJSON = new File(PATH_TO_JSON);
    @Autowired
    public Orders orders;
    public ManageOrderJson(Orders orders) {
        super(orders);
    }

    @Override
    public Order readById(String id) {
        LinkedList<Order> orders = null;
        Order order=null;
        try {
            FileReader fr = new FileReader(toJSON);
            BufferedReader reader = new BufferedReader(fr);
            String s = "";
            while (reader.ready()) {
                s += reader.readLine();
            }
            orders = gson.fromJson(s,new TypeToken<LinkedList<Order>>(){}.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Optional<Order> orderOpt = orders.stream().filter(i->((Order)i).getId().toString().equals(id)).findFirst();
        if(orderOpt.isPresent()){
            order = orderOpt.get();
        }
        logger.info("ReadByID operation");
        return order;
    }

    @Override
    public String saveById(String id) {
        //LinkedList<Order> orders = new LinkedList<>();
        String s = "";
        Optional<Order> orderOpt = this.orders.getOrders().stream().filter(i->((Order)i).getId().toString().equals(id)).findFirst();
        if(orderOpt.isPresent()){
            Order order = orderOpt.get();
            //orders.purchase(order);
            s = gson.toJson(orders);
            saveAll();
//            try {
//                FileWriter fw = new FileWriter(toJSON);
//                fw.write(s);
//                fw.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
        logger.info("SaveById Operation");
        return s;
    }

    @Override
    public LinkedList<Order> readAll() {
        LinkedList<Order> orders = null;
        try {
            FileReader fr = new FileReader(toJSON);
            BufferedReader reader = new BufferedReader(fr);
            String s = "";
            while (reader.ready()) {
                s += reader.readLine();
            }
            orders = gson.fromJson(s,new TypeToken<LinkedList<Order>>(){}.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("ReadAll Operation");
        return orders;
    }

    @Override
    public String saveAll() {
        String s = gson.toJson(orders.getOrders());
        try {
            FileWriter fw = new FileWriter(toJSON);
            fw.write(s);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("SaveAll operation");
        return s;
    }

    public static String convertOrderToJString(Order order){
        return gson.toJson(order);
    }

    public static Order convertJStringToOrder(String json){
        return gson.fromJson(json,Order.class);
    }

    public void deleteById(String id){
        orders.deleteOrder(id);
        saveAll();
        logger.info("DeleteById Operation.");
    }

    public String addToCard(String id){
        for (Object o:orders.getOrders()) {
            if(((Order)o).getBucket().getId().toString().equals(id)){
                Cap cap = new Cap();
                cap.create();
                ((Order) o).getBucket().add(cap);
                saveAll();
                logger.info("AddToCard operation");
                return cap.id.toString();
            }
        }
        return "No";
    }

}
