package application.orders;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;
@Entity
@Table(name="credentials")
public class Credentials implements Serializable {
    @Id
    @Column(name ="id")
    private String id;
    @Column(name ="first_name")
    private String firstName;
    @Column(name ="second_name")
    private String secondName;
    @Column(name ="father_name")
    private String fatherName;
    @Column(name ="email")
    private String email;

    public Credentials(){
        this.id = UUID.randomUUID().toString();
    }

    public Credentials(String firstName, String secondName, String fatherName, String email) {
        this();
        this.firstName = firstName;
        this.secondName = secondName;
        this.fatherName = fatherName;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public String getEmail() {
        return email;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
