package application.orders.checking;

import application.orders.Order;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CheckDone<T extends Order> extends ACheck {
    public List<T> orders;
    public CheckDone(List<T> orders){
        this.orders=orders;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(5000);
                checkDone(orders);
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
