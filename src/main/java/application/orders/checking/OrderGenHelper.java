package application.orders.checking;

import application.orders.Credentials;
import application.orders.Order;
import application.orders.ShoppingCart;
import application.wears.Cap;
import application.wears.SportWear;
import application.wears.TShirt;

public class OrderGenHelper {
    private Cap cap = new Cap();
    private TShirt shirt = new TShirt();
    private ShoppingCart<SportWear> cart = new ShoppingCart<>();

    public static  Credentials fakeCred() {
        return new Credentials();
    }

    public ShoppingCart fakeCart() {
        ShoppingCart cart = new ShoppingCart();
        cap.create();
        shirt.create();
        cart.add(cap);
        cart.add(shirt);
        return cart;
    }

    public Order createFakeOrder(){
        ShoppingCart cart = fakeCart();
        Credentials cred = fakeCred();
        return new Order(cart,cred);

    }
}
