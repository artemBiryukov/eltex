package application.orders.checking;

import application.orders.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.List;

public class CheckWait<T extends Order> extends ACheck {
    public List<T> orders;
    public CheckWait(List<T> orders){
        this.orders=orders;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(1000);
                checkWait(orders);
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
