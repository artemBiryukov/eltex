package application.orders.checking;

import application.orders.Order;
import application.orders.OrderStatus;

import java.util.Iterator;
import java.util.List;

public abstract class ACheck<T extends Order> extends Thread {

    public void checkDone(List<T> orders) {
        synchronized (orders) {
            Iterator<T> iterator = orders.iterator();
            while (iterator.hasNext()) {
                T order = iterator.next();
                if (order.status == OrderStatus.DONE) {
                    iterator.remove();
                   // System.out.println(order.getId() + " removed.");
                }
            }
        }
    }

    public void checkWait(List<T> orders) {
        synchronized (orders) {
            for (T order : orders) {
                if (order.status == OrderStatus.WAITING) {
                    order.status = OrderStatus.DONE;
                   // System.out.println(order.getId() + " " + order.status);
                }
            }
        }
    }
}
