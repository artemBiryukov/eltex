package application.orders.checking;

import application.orders.Credentials;
import application.orders.Order;
import application.orders.Orders;
import application.orders.ShoppingCart;
import org.springframework.beans.factory.annotation.Autowired;

public class OrderGen<T extends Orders> extends Thread {
    @Autowired
    public Orders orders;
    OrderGenHelper helper = new OrderGenHelper();

    public OrderGen(Orders orders) {
        this.orders = orders;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Credentials cred = helper.fakeCred();
                ShoppingCart cart = helper.fakeCart();
                Order order = new Order(cart,cred);
                orders.purchase(cred,cart);
                System.out.println("new order added");
                Thread.sleep(1500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
