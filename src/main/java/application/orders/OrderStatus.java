package application.orders;

public enum OrderStatus {
    WAITING,
    DONE
}
