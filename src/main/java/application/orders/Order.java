package application.orders;

import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "orders")
public class Order implements Serializable {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "status")
    public OrderStatus status;

    @Column(name = "creation_time")
    public Date creationTime;
    @Transient
    protected static long awaitingTime = 24 * 60 * 60 * 1000;

    @ManyToOne(optional = false)
    @JoinColumn(name="cred_id")
    private Credentials credentials;

    @OneToOne(optional = false)
    @JoinColumn(name="cart_id")
    private ShoppingCart shoppingCart;

    public ShoppingCart getBucket() {
        return shoppingCart;
    }

    public Order() {
        this.id = UUID.randomUUID().toString();
        this.creationTime = new Date();
        this.status = OrderStatus.WAITING;
    }

    public Order(ShoppingCart bucket, Credentials credentials) {
        this();
        this.shoppingCart = bucket;
        this.credentials = credentials;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public static long getAwaitingTime() {
        return awaitingTime;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public static void setAwaitingTime(long awaitingTime) {
        Order.awaitingTime = awaitingTime;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", status=" + status +
                ", creationTime=" + creationTime +
                ", credentials=" + credentials +
                ", bucket=" + shoppingCart +
                '}';
    }
}
