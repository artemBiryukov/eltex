package application.orders;

import java.io.Serializable;
import java.util.*;

public class Orders<T extends Order> implements Serializable {
    private List<T> orders = new LinkedList<>();

    private Map<Date, T> orderTime = new HashMap<>();

    public List<T> getOrders() {
        return orders;
    }

    public void purchase(Credentials credentials, ShoppingCart shoppingCart) {
        Order order = new Order(shoppingCart, credentials);
        synchronized (orders) {
            orders.add((T) order);
            orderTime.put(new Date(), (T) order);
        }
    }

    public void purchase(T order) {
        synchronized (orders) {
            orders.add(order);
        }
    }

    public void showAll() {
        for (T obj : orders) {
            System.out.println(obj);
        }
    }

    public void update() {
        Iterator<Map.Entry<Date, T>> iterator = orderTime.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Date, T> entry = iterator.next();
            Date creationDate = entry.getKey();
            T order = entry.getValue();
            if (order.status == OrderStatus.DONE || (new Date().getTime() - creationDate.getTime()) > Order.awaitingTime) {
                iterator.remove();
                orders.remove(order);
            }
        }
    }

    public Order findId(String id) {
        for (Object o : orders) {
            if(((Order)o).getId().toString().equals(id)){
                return (Order)o;
            }
        }
        return null;
    }

    public ShoppingCart findCart(String id){
        for (Object o:orders) {
            if(((Order)o).getBucket().getId().toString().equals(id)){
                return ((Order) o).getBucket();
            }
        }
        return null;
    }

    public void deleteOrder(String id){
        orders.remove(findId(id));
    }
}
