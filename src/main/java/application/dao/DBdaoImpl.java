package application.dao;

import application.orders.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class DBdaoImpl implements DBdao {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Order> readAll() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Order> list = session.createQuery("from Order").list();
        for (Order o :list) {
            System.out.println(o);
        }
        return list;
    }

    @Override
    public Order readById(String id) {
        Session session = this.sessionFactory.getCurrentSession();
        Order order = session.load(Order.class,id);
        return order;
    }

    @Override
    public void addToCart(String id) {

    }

    @Override
    public void delById(String id) {
        Session session = this.sessionFactory.getCurrentSession();
        Order order = session.load(Order.class,id);
        session.delete(order);
    }
}
