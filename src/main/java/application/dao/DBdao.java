package application.dao;

import application.orders.Order;

import java.util.List;

public interface DBdao {
    List readAll();
    Order readById(String id);
    void addToCart(String id);
    void delById(String id);


}
