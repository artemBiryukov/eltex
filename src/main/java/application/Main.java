package application;

import application.wears.Cap;
import application.wears.TShirt;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
@SpringBootApplication
@EnableJpaAuditing
public class Main {

    public static void main(String[] args) throws InterruptedException {
        LabSpring initObject = new LabSpring();
        SpringApplication.run(Main.class, args);
        Thread.sleep(3000);
        initObject.init();
        initObject.serverStart();
    }


    public void firstLab(String[] args) {
        int count = Integer.parseInt(args[0]);
        String view = args[1].toLowerCase();
        int i = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (i < count) {
            try {
                String name = reader.readLine();
                String firm = reader.readLine();
                double price = Double.parseDouble(reader.readLine());
                if (view.equals("tshirt")) {
                    new TShirt(name, firm, price).read();
                } else if (view.equals("cap")) {
                    new Cap(name, firm, price);
                } else {
                    System.out.println("Error");
                    break;
                }
                i++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
