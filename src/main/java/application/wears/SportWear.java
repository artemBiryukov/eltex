package application.wears;


import application.orders.ShoppingCart;

import javax.persistence.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "wear")
public class SportWear implements ICrudAction, Serializable {
    @Id
    public UUID id;
    public static int count = 0;
    @Column(name = "name")
    protected String name;
    @Column (name = "price")
    protected double price;
    @Column(name="firm")
    protected String firm;
    @ManyToOne(fetch=FetchType.LAZY,optional = false)
    @JoinColumn(name = "cart_id")
    public ShoppingCart cart;

    public SportWear() {
        this.id = UUID.randomUUID();
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public void create() {
        count++;
        this.id = UUID.randomUUID();
        this.firm = "firm" + Math.random() * 100;
        this.name = "name" + Math.random() * 100;
        this.price = (Math.random() * 10);
    }

    public void read() {
        //System.out.println(String.format("id = %s, Name = %s, Price = %s, Firm = %s", this.id, this.name, this.price, this.firm));
        System.out.println(this.toString());
    }

    public void update() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            this.name = reader.readLine();
            this.price = Double.parseDouble(reader.readLine());
            this.firm = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        this.firm = null;
        this.name = null;
        this.price = 0;
        this.id = null;
        count--;
    }

    @Override
    public String toString() {
        return "SportWear{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", firm='" + firm + '\'' +
                '}';
    }
}
