package application.wears;

public class Cap extends SportWear {

    public Cap(){
    }

    public Cap(String name, String firm, double price) {
        super();
        count++;
        this.name = name;
        this.firm = firm;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Cap{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", firm='" + firm + '\'' +
                '}';
    }
}
