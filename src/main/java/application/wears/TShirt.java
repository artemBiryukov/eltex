package application.wears;

public class TShirt extends SportWear {

    public TShirt(){
    }

    public TShirt(String name, String firm, double price) {
        super();
        count++;
        this.name = name;
        this.firm = firm;
        this.price = price;
    }

    @Override
    public String toString() {
        return "TShirt{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", firm='" + firm + '\'' +
                '}';
    }
}
