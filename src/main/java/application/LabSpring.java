package application;

import application.orders.Credentials;
import application.orders.Order;
import application.orders.Orders;
import application.orders.ShoppingCart;
import application.sockets.Client;
import application.sockets.Server;
import application.wears.Cap;
import application.wears.SportWear;
import application.wears.TShirt;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LabSpring {
    Orders orders;
    @Bean
    public Orders init() {
        TShirt t = new TShirt();
        Cap cap1 = new Cap();
        t.create();
        cap1.create();
        Credentials firstCred = new Credentials("Name", "surName", "fatherName", "email");
        ShoppingCart<SportWear> cart = new ShoppingCart<>();
        cart.add(t);
        cart.add(cap1);
        orders = new Orders();
        orders.purchase(firstCred, cart);
        orders.purchase(firstCred, cart);
        return orders;
    }

    public void serverStart() {
        Server server = new Server(orders, 5000, 8100, "localhost");
        server.start();
        Order order = new Order();
        Client client = new Client(order, 8100);
        Client client1 = new Client(new Order(), 8100);
        Client client2 = new Client(new Order(), 8100);
        Client client3 = new Client(new Order(), 8100);
        Client client4 = new Client(new Order(), 8100);
        client.start();
        client1.start();
        client2.start();
        client3.start();
        client4.start();
    }
}
