package application.controller;

import application.ExceptionHandler;
import application.orders.Order;
import application.orders.Orders;
import application.orders.ShoppingCart;
import application.orders.serialize.ManageOrderJson;
import application.repositories.CredentialsRepository;
import application.repositories.OrderRepository;
import application.repositories.ShoppingCartRepository;
import application.repositories.SportWearRepository;
import application.wears.SportWear;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RestController
public class Controller {
    GsonBuilder gsonBuilder = new GsonBuilder();
    Gson gson = gsonBuilder.create();

    @Autowired
    private Orders orders;
    @Autowired
    ManageOrderJson moj;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ShoppingCartRepository shoppingCartRepository;
    @Autowired
    CredentialsRepository credentialsRepository;
    @Autowired
    SportWearRepository sportWearRepository;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    private String readAll(@RequestParam(value = "command") String s, @RequestParam(value = "order_id", required = false) String id, @RequestParam(value = "card_id", required = false) String cardId) {
        if (s.equals("readall")) {
            ArrayList<Order> list = (ArrayList<Order>) orderRepository.findAll();
            return gson.toJson(list);
            //return moj.saveAll();
        } else if (s.equals("readById")) {
            Optional orderOpt = orderRepository.findById(id);
            if(orderOpt.isPresent()){
                Order order = (Order)orderOpt.get();
                return gson.toJson(order);
            }
            return ManageOrderJson.convertOrderToJString(moj.readById(id));
        } else if (s.equals("addToCard")) {
            Optional cartOpt = shoppingCartRepository.findById("1");
            if (cartOpt.isPresent()) {
                ShoppingCart cart = (ShoppingCart) cartOpt.get();
                SportWear wear = new SportWear();
                wear.setCart(cart);
                sportWearRepository.save(wear);
                return wear.getId().toString();
            }
            return moj.addToCard(cardId);
        } else if (s.equals("delById")) {
            Optional orderOpt = orderRepository.findById(id);
            if(orderOpt.isPresent()){
                orderRepository.deleteById(id);
                return "0";
            }
            return ExceptionHandler.deleteAndHandle(orders, s, id, moj);
        }
        return "Unknown exception.";
    }
}
