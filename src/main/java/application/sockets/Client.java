package application.sockets;

import application.orders.Order;
import application.orders.checking.OrderGenHelper;
import application.orders.serialize.ManageOrderJson;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Client extends Thread {
    public static AtomicInteger count = new AtomicInteger(-1);
    public OrderGenHelper orderGen;
    private Order order;
    private int portUDPAnswer = 9000;
    private DatagramSocket datagramSocket;
    private DatagramSocket socketAns;
    private DatagramPacket packet;
    private Socket socket;
    private int portUDP;

    public Client(Order order, int portUDP) {
        this.order = order;
        this.portUDP = portUDP;
        this.portUDPAnswer = portUDPAnswer + count.incrementAndGet();
        this.orderGen = new OrderGenHelper();
        try {
            this.socketAns = new DatagramSocket(portUDPAnswer);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        String s = "";
        s = receiveUDP();
        sendTCP(s, order);
        waitForAnswer();
    }

    public String receiveUDP() {
        byte[] buf = new byte[1000];
        String info = "";
        try {
            datagramSocket = new DatagramSocket(null);
            datagramSocket.setBroadcast(true);
            datagramSocket.setReuseAddress(true);
            datagramSocket.bind(new InetSocketAddress(portUDP));
        } catch (SocketException e) {
            e.printStackTrace();
        }
        packet = new DatagramPacket(buf, buf.length);
        try {
            datagramSocket.receive(packet);
            info = new String(packet.getData(), 0, packet.getLength());
            datagramSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return info;
    }

    public void sendTCP(String info, Order order) {
        if (info.contains(":")) {
            String[] mass = info.split(":");
            String address = mass[0];
            int portTCP = Integer.parseInt(mass[1]);
            try {
                socket = new Socket(address, portTCP);
                socket.getOutputStream().write((ManageOrderJson.convertOrderToJString(order) + "//" + portUDPAnswer).getBytes());
                socket.getOutputStream().flush();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println(info);
        }
    }

    public void waitForAnswer() {
        try {
            byte[] buf = new byte[1000];
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            socketAns.receive(packet);
            String info = new String(buf, 0, packet.getLength());
            System.out.println(info);
            //generateOrders();// Uncomment if u want to turn on order generation.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void generateOrders() {
        try {
            sleep(1000);
            String s;
            s = receiveUDP();
            Order fake = orderGen.createFakeOrder();
            sendTCP(s, fake);
            waitForAnswer();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

