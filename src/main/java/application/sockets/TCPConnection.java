package application.sockets;

import application.orders.Order;
import application.orders.OrderStatus;
import application.orders.Orders;
import application.orders.serialize.ManageOrderJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.Socket;
import java.util.Date;
public class TCPConnection extends Thread {
    @Autowired
    private Orders orders;
    private Socket socket;

    public TCPConnection(Orders orders, Socket socket) {
        this.orders = orders;
        this.socket = socket;
        start();
    }

    @Override
    public void run() {
        byte[] buf = new byte[1000];
        try {
            int r = socket.getInputStream().read(buf);
            String s = new String(buf, 0, r);
            String[] mass = s.split("//");
            Order order = ManageOrderJson.convertJStringToOrder(mass[0]);
            orders.purchase(order);
            int port = Integer.parseInt(mass[1]);
            //System.out.println(port);//print answer port for client
            //System.out.println(order);//received order
            socket.close();
            check(order, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void check(Order order, int portUDPAnswer) {
        boolean flag = true;
        while (flag) {
            for (Object o : orders.getOrders()) {
                if (o.equals(order) && ((Order) o).status == OrderStatus.DONE) {
                    long time = new Date().getTime() - ((Order) o).creationTime.getTime();
                    String s = String.format("Zakaz %s obrabotan za %d ms.",((Order)o).getId().toString(),time);
                    ServerUDP.send(s, portUDPAnswer);
                    flag = false;
                    break;
                }
            }
            try {
                sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
