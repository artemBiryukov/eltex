package application.sockets;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ServerUDP extends Thread {
    private static int portUDP;
    private static int portTCP;
    private static DatagramSocket datagramSocket;
    private static String address;

    public ServerUDP(int portUDP, int portTCP, String address) {
        ServerUDP.portUDP = portUDP;
        ServerUDP.portTCP = portTCP;
        ServerUDP.address = address;
    }

    public static void send(String info, int portUDP) {
        byte[] buf = info.getBytes();
        try {
            datagramSocket = new DatagramSocket();
            datagramSocket.setBroadcast(true);
            DatagramPacket datagramPacket = new DatagramPacket(buf, buf.length, InetAddress.getByName(address), portUDP);
            datagramSocket.send(datagramPacket);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        String info = address + ":" + portTCP;
        while (true) {
            try {
                //System.out.println("sending info po UDP");
                send(info, portUDP);
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}
