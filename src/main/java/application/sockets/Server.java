package application.sockets;

import application.orders.Orders;
import application.orders.checking.CheckDone;
import application.orders.checking.CheckWait;
import org.springframework.beans.factory.annotation.Autowired;

public class Server extends Thread {
    @Autowired
    public Orders orders;
    private int portUDP;
    private int portTCP;
    private String address;

    public Server(Orders orders, int portTCP, int portUDP, String address) {
        this.orders = orders;
        this.portTCP = portTCP;
        this.portUDP = portUDP;
        this.address = address;
    }

    @Override
    public void run() {
        ServerUDP serverUDP = new ServerUDP(portUDP, portTCP, address);
        ServerTCP serverTCP = new ServerTCP(orders, 5000);
        CheckWait checkWait = new CheckWait(orders.getOrders());
        checkWait.start();
        CheckDone checkDone = new CheckDone(orders.getOrders());
        checkDone.start();
        serverUDP.start();
        serverTCP.start();
    }


}
