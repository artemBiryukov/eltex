package application.sockets;

import application.orders.Orders;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerTCP extends Thread {
    @Autowired
    Orders orders;
    private ServerSocket serverSocket;

    public ServerTCP(Orders orders, int portTCP) {
        this.orders = orders;
        try {
            this.serverSocket = new ServerSocket(portTCP);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                Socket in = serverSocket.accept();
                new TCPConnection(orders, in);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
